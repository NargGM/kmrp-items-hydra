package de.cas_ual_ty.gci.inventory.inventory.capabilities;


import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.inventory.bigcontainer.capabilities.CAPContainerBigProvider;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

import javax.annotation.Nonnull;

public class ContainerCapabilityEventHandler {

    public static void register(){
        MinecraftForge.EVENT_BUS.register(new ContainerCapabilityEventHandler());
    }

    public static final ResourceLocation CONTAINER_CAP = new ResourceLocation(GunCus.MOD_ID, "inventory");
    public static final ResourceLocation CONTAINERBIG_CAP = new ResourceLocation(GunCus.MOD_ID, "inventorybig");

    //ОЧЕНЬ ВАЖНО! Добавляет капу игроку при его первом создании

    @SubscribeEvent
    public void attachCapability(@Nonnull AttachCapabilitiesEvent<ItemStack>  event) {
/*        event.addCapability(CONTAINER_CAP, new CAPContainerProvider());
        event.addCapability(CONTAINERBIG_CAP, new CAPContainerBigProvider());*/
    }



    //Копирование инвентаря, если по каким-то причинам произошло клонирование игрока. Иначе вещи пропадут
/*    @SubscribeEvent
    public void onPlayerClone(PlayerEvent.Clone event) {
        EntityPlayer player = event.getEntityPlayer();
        ICAPContainer newCap = player.getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        ICAPContainer oldCap = event.getOriginal().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        newCap.copyInventory(oldCap);
    }*/

/*    //Если игрок умрет то ничего не выпадет. Нужно выбросит вещи вручную. Выбрасываем
    @SubscribeEvent
    public void onPlayerDeath(LivingDeathEvent event) {
        if(event.getEntity() instanceof EntityPlayer) {
            //Достаем КАПу, затем инвентарь
            EntityPlayer player = (EntityPlayer)event.getEntity();
            ICAPCustomInventory cap = player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            //Выбрасываем все вещи из инвентаря
            dropAllItems(player, inv);
            inv.clear();
        }
    }

    private static void dropAllItems(EntityPlayer player, CustomInventory inventory){
        NonNullList<ItemStack> aitemstack = inventory.getStacks();
        for (int i = 0; i < aitemstack.size(); ++i) {
            if (!aitemstack.get(i).isEmpty()) {
                player.dropItem(aitemstack.get(i), true, false);
            }
        }
    }*/
}


