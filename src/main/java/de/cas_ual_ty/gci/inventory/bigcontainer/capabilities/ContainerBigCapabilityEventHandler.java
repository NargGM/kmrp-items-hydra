package de.cas_ual_ty.gci.inventory.bigcontainer.capabilities;


import de.cas_ual_ty.gci.GunCus;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent;

import javax.annotation.Nonnull;

public class ContainerBigCapabilityEventHandler {

    public static void register(){
        MinecraftForge.EVENT_BUS.register(new ContainerBigCapabilityEventHandler());
    }

    public static final ResourceLocation CONTAINERBIG_CAP = new ResourceLocation(GunCus.MOD_ID, "inventory");

    //ОЧЕНЬ ВАЖНО! Добавляет капу игроку при его первом создании

    @SubscribeEvent
    public void attachCapability(@Nonnull AttachCapabilitiesEvent<ItemStack>  event) {
        event.addCapability(CONTAINERBIG_CAP, new CAPContainerBigProvider());
    }

/*    @SubscribeEvent
    public void containerCopy(PlayerEvent.ItemPickupEvent event) {
        ICAPContainerBig oldCap = event.getOriginalEntity().getCapability(CAPContainerBigProvider.CONTAINERBIG_CAP, null);
        ICAPContainerBig newCap = event.getStack().getCapability(CAPContainerBigProvider.CONTAINERBIG_CAP, null);
        if (newCap != null) {
            newCap.copyInventory(oldCap);
        }
    }*/
    //Копирование инвентаря, если по каким-то причинам произошло клонирование игрока. Иначе вещи пропадут
/*    @SubscribeEvent
    public void onPlayerClone(PlayerEvent.Clone event) {
        EntityPlayer player = event.getEntityPlayer();
        ICAPContainer newCap = player.getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        ICAPContainer oldCap = event.getOriginal().getCapability(CAPContainerProvider.CONTAINER_CAP, null);
        newCap.copyInventory(oldCap);
    }*/

/*    //Если игрок умрет то ничего не выпадет. Нужно выбросит вещи вручную. Выбрасываем
    @SubscribeEvent
    public void onPlayerDeath(LivingDeathEvent event) {
        if(event.getEntity() instanceof EntityPlayer) {
            //Достаем КАПу, затем инвентарь
            EntityPlayer player = (EntityPlayer)event.getEntity();
            ICAPCustomInventory cap = player.getCapability(CAPCustomInventoryProvider.INVENTORY_CAP, null);
            CustomInventory inv = cap.getInventory();
            //Выбрасываем все вещи из инвентаря
            dropAllItems(player, inv);
            inv.clear();
        }
    }

    private static void dropAllItems(EntityPlayer player, CustomInventory inventory){
        NonNullList<ItemStack> aitemstack = inventory.getStacks();
        for (int i = 0; i < aitemstack.size(); ++i) {
            if (!aitemstack.get(i).isEmpty()) {
                player.dropItem(aitemstack.get(i), true, false);
            }
        }
    }*/
}


