package de.cas_ual_ty.gci.inventory.bigcontainer.capabilities;

import de.cas_ual_ty.gci.inventory.bigcontainer.ContainerBigC;

public interface ICAPContainerBig {

    public void copyInventory(ICAPContainerBig inventory);
    public ContainerBigC getInventory();
}