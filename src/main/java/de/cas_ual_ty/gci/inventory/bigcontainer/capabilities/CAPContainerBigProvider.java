package de.cas_ual_ty.gci.inventory.bigcontainer.capabilities;



import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

public class CAPContainerBigProvider implements ICapabilitySerializable<NBTBase> {

    //Инициализация КАПы с помощью аннотации
    @CapabilityInject(ICAPContainerBig.class)
    public static final Capability<ICAPContainerBig> CONTAINERBIG_CAP = null;

    private ICAPContainerBig instance = CONTAINERBIG_CAP.getDefaultInstance();

    //Метод что осуществляет проверку на наличие КАПы
    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing) {
        return capability == CONTAINERBIG_CAP;
    }

    //Метод что осуществляет доступ к КАПе
    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing) {
        return capability == CONTAINERBIG_CAP ? CONTAINERBIG_CAP.<T> cast(this.instance) : null;
    }

    //Метод инициации сохранения информации о инвентаре в НБТ
    @Override
    public NBTBase serializeNBT() {
        return CONTAINERBIG_CAP.getStorage().writeNBT(CONTAINERBIG_CAP, this.instance, null);
    }

    //Метод инициации чтения информации о инвентаре из НБТ
    @Override
    public void deserializeNBT(NBTBase nbt) {
        CONTAINERBIG_CAP.getStorage().readNBT(CONTAINERBIG_CAP, this.instance, null, nbt);
    }

}


