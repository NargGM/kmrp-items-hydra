package de.cas_ual_ty.gci.network;

import de.cas_ual_ty.gci.GunCus;
import de.cas_ual_ty.gci.SoundEventGCI;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class MessageSoundReg implements IMessage
{
	public int soundID;
	public float volume;
	public float pitch;

	public MessageSoundReg()
	{

	}

	public MessageSoundReg(int soundID, float volume, float pitch)
	{
		this.soundID = soundID;
		this.volume = volume;
		this.pitch = pitch;
	}

	public MessageSoundReg(SoundEventGCI sound, float volume, float pitch)
	{
		this(sound.getID(), volume, pitch);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.soundID = buf.readInt();
		this.volume = buf.readFloat();
		this.pitch = buf.readFloat();
	}
	
	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeInt(this.soundID);
		buf.writeFloat(this.volume);
		buf.writeFloat(this.pitch);
	}
	
	public static class MessageHandlerSoundReg implements IMessageHandler<MessageSoundReg, IMessage>
	{
		@Override
		public IMessage onMessage(MessageSoundReg message, MessageContext ctx)
		{
			EntityPlayer player = GunCus.proxy.getClientPlayer(ctx);
			
			if(player != null)
			{
				SoundEventGCI sound = SoundEventGCI.soundEventList[message.soundID];
				
				if(sound != null)
				{
					System.out.println("playing sound3" + player.world.isRemote);
					if(player.world.isRemote) player.playSound(sound, message.volume, message.pitch);
				}
			}
			
			return null;
		}
	}
}
